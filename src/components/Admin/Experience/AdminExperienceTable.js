import React from "react";
import TableHeader from "../../Table/TableHeader";
import TableRow from "../../Table/TableRow";
import TableCell from "../../Table/TableCell";
import Button from "../../Button/Button";
import DurationDate from "../../Timeline/Date/DurationDate";
import {DateTime} from "luxon";

const AdminExperienceTable = ({experience, onExperienceDeleteClick}) => {
	return (
		<div className="flex flex-col mx-4 mb-6 mt-6 lg:mx-32">
			<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
				<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
					<div className="shadow overflow-hidden border-b border-dark-200 rounded-lg">
						<table className="min-w-full divide-y divide-dark-200">
							<thead>
								<tr>
									<TableHeader
										text="Title"
									/>
									<TableHeader
										text="Company"
									/>
									<TableHeader
										text="URL"
									/>
									<TableHeader
										text="Start/End Date"
									/>
									<TableHeader
										text="Actions"
									/>
								</tr>
							</thead>
							<tbody className="divide-y divide-dark-200">
								{experience.map((experienceObject) => {
									let startDate = DateTime.fromMillis(experienceObject.startDate);
									let endDate = experienceObject.endDate ?
										DateTime.fromMillis(experienceObject.endDate) :
										null;

									return (
										<TableRow key={experienceObject.id}>
											<TableCell>
												<div className="flex-shrink-0 h-10 w-10">
													<img className="object-cover object-center"
														src={experienceObject.iconUrl} alt=""/>
												</div>
												<div className="ml-4">
													<div className="text-sm font-medium">
														{experienceObject.title}
													</div>
												</div>
											</TableCell>

											<TableCell>
												{experienceObject.company}
											</TableCell>

											<TableCell>
												{experienceObject.url}
											</TableCell>

											<TableCell>
												<DurationDate
													startDate={experienceObject.startDate}
													startDateString={startDate.toFormat("MMMM yyyy")}
													endDate={experienceObject.endDate}
													endDateString={endDate ? endDate.toFormat("MMMM yyyy") : null}
													includeDiff={false}
												/>
											</TableCell>

											<TableCell>
												<Button
													colour="yellow"
													textColour="gray-800"
													href={`/admin/experience/edit/${experienceObject.id}`}
												>
													Edit
												</Button>
												<Button
													colour="red"
													textColour="white"
													onClick={() => onExperienceDeleteClick(experienceObject.id)}
												>
													Delete
												</Button>
											</TableCell>
										</TableRow>
									);
								})}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	);
};

export default AdminExperienceTable;
