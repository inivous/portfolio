import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import EditExperienceForm from "../../Form/EditExperienceForm";
import {DateTime} from "luxon";
import DurationDate from "../../Timeline/Date/DurationDate";
import TimelineElement from "../../Timeline/TimelineElement";

const EditExperience = () => {
	const router = useRouter();
	let {id} = router.query;

	if (!id)
		return null;

	const [experience, setExperience] = useState(null);
	const {
		experience: currentExperience,
		loading: experienceLoading
	} = useEndpoint(`/api/experience/${id}`, "experience", true, false);

	useEffect(() => {
		if (currentExperience)
			setExperience(currentExperience);
	}, [currentExperience]);

	if (experienceLoading || !experience) {
		return (
			<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

				<div className="flex justify-center py-12 px-0 lg:px-8">
					<div className="max-w-md w-full space-y-8 mt-8">
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="2.875rem" className="my-2"/>
						<Skeleton height="3rem" className="my-2"/>
						<Skeleton height="2.375rem" className="my-2"/>
					</div>
				</div>

				<div>
					<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
						Preview
					</h1>

					<h1 className="mb-4 ml-4">
						<Skeleton width="30%"/>
					</h1>
					<Skeleton height="20rem"/>
				</div>
			</div>
		);
	}

	let iconBackgroundColour = "#252525";

	if (experience.iconUrl)
		iconBackgroundColour = "#ffffff00";

	let startDate = DateTime.fromMillis(experience.startDate);
	let endDate = experience.endDate ?
		DateTime.fromMillis(experience.endDate) :
		null;
	let dateElement = <DurationDate
		startDate={experience.startDate}
		startDateString={startDate.toFormat("MMMM yyyy")}
		endDate={experience.endDate}
		endDateString={endDate ? endDate.toFormat("MMMM yyyy") : null}
	/>;

	return (
		<div className="grid grid-cols-1 text-center px-8 gap-4 mb-4
				lg:grid-cols-2 lg:px-24">

			<EditExperienceForm
				onChange={(newValue) => setExperience({
					...experience,
					[newValue.name]: newValue.value
				})}
				baseExperience={experience}
			/>

			<div>
				<h1 className="text-3xl font font-extrabold tracking-tight text-white mb-4">
					Preview
				</h1>

				<TimelineElement
					backgroundColour="#252525"
					iconBackgroundColour={iconBackgroundColour}
					iconElement={<img src={experience.iconUrl} alt="Icon"/>}
					startDate={experience.startDate}
					startDateString={startDate.toFormat("MMMM yyyy")}
					endDate={endDate}
					dateElement={dateElement}
					title={experience.title}
					location={experience.company}
					body={experience.description}
					url={experience.url}
				/>
			</div>
		</div>
	);
};

export default EditExperience;
