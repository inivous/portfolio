import React, {useState} from "react";
import BulletList from "../../BulletList/BulletList";
import BulletListItem from "../../BulletList/BulletListItem";
import Button from "../../Button/Button";
import AddImageModal from "../../Modal/AddImageModal";
import ImageURL from "../ImageURL/ImageURL";

const EditableImagesList = ({onChange, defaultImages = []}) => {
	const [images, setImages] = useState(defaultImages);
	const [addImageOpen, setAddImageOpen] = useState(false);

	const handleAddImage = (image) => {
		let newImages = [...images, image];
		setImages(newImages);
		setAddImageOpen(false);
		onChange(newImages);
	};

	return (
		<div className="mt-4">
			<h1 className="font-bold text-left text-xl">
				Images
			</h1>
			<div className="mx-10 my-4 text-left">
				<BulletList>
					{images.map((image, index) => {
						return <BulletListItem key={index}>
							<ImageURL
								url={image}
								onDelete={() => {
									let newImages = images.filter(x => !(x === image));
									setImages(newImages);
									onChange(newImages);
								}}
							/>
						</BulletListItem>;
					})}
				</BulletList>
			</div>

			<div className="mt-6 mb-6">
				<Button colour="green" onClick={() => setAddImageOpen(true)}>
					Add Image
				</Button>
			</div>

			<AddImageModal
				isOpen={addImageOpen}
				onClose={() => setAddImageOpen(false)}
				onAdd={handleAddImage}
			/>
		</div>
	);
};

export default EditableImagesList;
