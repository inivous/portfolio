import React, {useState} from "react";
import useEndpoint from "../../../lib/useEndpoint";
import Skeleton from "react-loading-skeleton";
import AdminSkillGroup from "./AdminSkillGroup";
import DeleteSkillModal from "../../Modal/DeleteSkillModal";
import deleteSkill from "../../../helpers/apiMethods/skills/deleteSkill";
import {useAlert} from "react-alert";
import {mutate} from "swr";
import Button from "../../Button/Button";

const AdminSkillsList = () => {
	const alert = useAlert();
	const {skills, loading: skillsLoading} = useEndpoint("/api/skill", "skills", true, false);

	const [isOpen, setIsOpen] = useState(false);
	const [selectedSkillToDelete, setSelectedSkillToDelete] = useState(false);
	const [skillDeleteLoading, setSkillDeleteLoading] = useState(false);

	if (skillsLoading) {
		return (
			<div className="grid grid-cols-1 text-center px-8 gap-4
				lg:grid-cols-3 2xl:grid-cols-4 lg:px-32">
				{Array(6).fill(null).map((d, i) => {
					return <Skeleton
						key={i}
						height="12rem"
					/>;
				})}
			</div>
		);
	}

	const handleSkillDeleteClicked = (skillId) => {
		setIsOpen(true);
		setSelectedSkillToDelete(skillId);
	};

	const handleSkillDeleteConfirm = async () => {
		setSkillDeleteLoading(true);

		let {error} = await deleteSkill(selectedSkillToDelete);

		if (error) {
			setSkillDeleteLoading(false);
			alert.error(`Failed to delete skill: ${error.displayMessage}`);
			return;
		}

		await mutate("/api/skill");
		setSkillDeleteLoading(false);
		setIsOpen(false);
		alert.success("Successfully deleted skill");
	};

	let skillGroups = {};

	skills.forEach(skill => {
		if (!skillGroups[skill.type])
			skillGroups[skill.type] = [];

		skillGroups[skill.type].push(skill);
	});

	return (
		<>
			<div className="mb-1 ml-4 md:ml-8 lg:ml-16 px-2 sm:px-4 lg:px-6 lg:mb-8">
				<Button colour="green" href="/admin/skills/new">
					Create New
				</Button>
			</div>
			{Object.keys(skillGroups).sort().map(groupName => {
				return <AdminSkillGroup
					key={groupName}
					title={groupName}
					skills={skillGroups[groupName]}
					onSkillDeleteClick={handleSkillDeleteClicked}
				/>;
			})}

			<DeleteSkillModal
				isOpen={isOpen}
				onClose={() => setIsOpen(false)}
				onDelete={handleSkillDeleteConfirm}
				deleteLoading={skillDeleteLoading}
			/>
		</>
	);
};

export default AdminSkillsList;
