import React from "react";
import TimelineElement from "../Timeline/TimelineElement";
import Timeline from "../Timeline/Timeline";
import useEndpoint from "../../lib/useEndpoint";
import {DateTime} from "luxon";
import DurationDate from "../Timeline/Date/DurationDate";

const ExperienceList = () => {
	const {experience, loading: experienceLoading} = useEndpoint("/api/experience", "experience");

	if (experienceLoading) {
		return (
			<div className="text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
				<Timeline>
					{Array(2).fill().map((val, i) => {
						return <TimelineElement
							key={i}
							loading={true}
							backgroundColour="#202020"
							iconBackgroundColour="#202020"
						/>;
					})}
				</Timeline>
			</div>
		);
	}

	return (
		<div className="text-center px-8 gap-4 lg:grid-cols-3 lg:px-32">
			<Timeline>
				{experience.map(experienceObject => {
					let startDate = DateTime.fromMillis(experienceObject.startDate);
					let endDate = experienceObject.endDate ?
						DateTime.fromMillis(experienceObject.endDate) :
						null;

					let dateElement = <DurationDate
						startDate={experienceObject.startDate}
						startDateString={startDate.toFormat("MMMM yyyy")}
						endDate={experienceObject.endDate}
						endDateString={endDate ? endDate.toFormat("MMMM yyyy") : null}
					/>;

					let iconBackgroundColour = "#252525";

					if (experienceObject.iconUrl)
						iconBackgroundColour = "#ffffff00";

					return <TimelineElement
						key={experienceObject.id}
						backgroundColour="#252525"
						iconBackgroundColour={iconBackgroundColour}
						iconElement={<img src={experienceObject.iconUrl} alt="Icon"/>}
						startDate={experienceObject.startDate}
						startDateString={startDate.toFormat("MMMM yyyy")}
						endDate={endDate}
						dateElement={dateElement}
						title={experienceObject.title}
						location={experienceObject.company}
						body={experienceObject.description}
						url={experienceObject.url}
					/>;
				})}
			</Timeline>
		</div>
	);
};

export default ExperienceList;
