import React, {useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	AcademicCapIcon,
	AdjustmentsIcon,
	ChartPieIcon,
	ClockIcon,
	LocationMarkerIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import {useRouter} from "next/router";
import NumberInput from "../FormElements/NumberInput";
import CreateEducationSchema from "../../schemas/Frontend/CreateEducationSchema";
import createEducation from "../../helpers/apiMethods/education/createEducation";

const NewEducationForm = ({onChange}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		if (["startDate", "endDate"].includes(name) && value === "")
			value = 1900;

		onChange({name, value});
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			subject: subjectElement,
			level: levelElement,
			location: locationElement,
			grade: gradeElement,
			startDate: startDateElement,
			endDate: endDateElement
		} = event.target;

		let subject = subjectElement.value;
		let level = levelElement.value;
		let location = locationElement.value;
		let grade = gradeElement.value;
		let startDate = startDateElement.value;
		let endDate = endDateElement.value;

		let validationResult = CreateEducationSchema.validate({
			subject,
			level,
			location,
			grade,
			startDate,
			endDate
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			subject,
			level,
			location,
			grade,
			startDate,
			endDate
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await createEducation(subject, level, location, grade, startDate, endDate);

		setLoading(false);

		if (error) {
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await router.push("/admin/education?addedEducation=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="subject"
						placeholder="Subject"
						position="alone"
						autoComplete="off"
						icon={AcademicCapIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="level"
						placeholder="Level"
						position="alone"
						autoComplete="off"
						icon={AdjustmentsIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="location"
						placeholder="Location"
						position="alone"
						autoComplete="off"
						icon={LocationMarkerIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="grade"
						placeholder="Grade"
						position="alone"
						autoComplete="off"
						icon={ChartPieIcon}
						onInput={handleOnInput}
					/>

					<div className="grid grid-cols-2">
						<NumberInput
							labelClassName="mr-2"
							name="startDate"
							placeholder="Start Year"
							position="alone"
							icon={ClockIcon}
							min={1900}
							max={2100}
							onInput={handleOnInput}
						/>

						<NumberInput
							labelClassName="ml-2"
							name="endDate"
							placeholder="End Year"
							position="alone"
							icon={ClockIcon}
							min={1900}
							max={2100}
							onInput={handleOnInput}
						/>
					</div>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Creating..." : "Create"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default NewEducationForm;
