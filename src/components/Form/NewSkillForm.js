import React, {useState} from "react";
import TextInput from "../FormElements/TextInput";
import {
	LinkIcon,
	PencilAltIcon,
	PhotographIcon,
	ViewListIcon
} from "@heroicons/react/solid";
import ErrorList from "../Error/ErrorList";
import SubmitButton from "../FormElements/SubmitButton";
import ColourPickerButton from "../ColourPicker/ColourPickerButton";
import CreateSkillSchema from "../../schemas/Frontend/CreateSkillSchema";
import createSkill from "../../helpers/apiMethods/skills/createSkill";
import {useRouter} from "next/router";

const NewSkillForm = ({onChange}) => {
	const router = useRouter();
	const [loading, setLoading] = useState(false);
	const [errors, setErrors] = useState([]);
	const [colour, setColour] = useState("ffffff");

	const handleOnInput = (event) => {
		let {name, value} = event.target;

		onChange({name, value});
	};

	const handleColourChange = (newColour) => {
		setColour(newColour);

		onChange({name: "colour", value: newColour});
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		let {
			name: nameElement,
			type: typeElement,
			link: linkElement,
			imageUrl: imageUrlElement
		} = event.target;

		let name = nameElement.value;
		let type = typeElement.value;
		let link = linkElement.value;
		let imageUrl = imageUrlElement.value;

		let validationResult = CreateSkillSchema.validate({
			name,
			type,
			link,
			imageUrl
		}, {
			abortEarly: false,
			stripUnknown: true
		});

		if (validationResult.error) {
			setErrors(validationResult.error.details);
			return;
		}

		({
			name,
			type,
			link,
			imageUrl
		} = validationResult.value);

		setErrors([]);
		setLoading(true);

		let {error} = await createSkill(name, type, link, imageUrl, colour);

		setLoading(false);

		if (error) {
			setErrors([{message: error.displayMessage}]);
			return;
		}

		await router.push("/admin/skills?addedSkill=1");
	};

	return (
		<div className="flex justify-center py-12 px-0 lg:px-8">
			<div className="max-w-md w-full space-y-8">
				<form className="mt-8 space-y-4" onSubmit={handleSubmit}>
					<TextInput
						name="name"
						placeholder="Name"
						position="alone"
						autoComplete="off"
						icon={PencilAltIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="type"
						placeholder="Type"
						position="alone"
						autoComplete="off"
						icon={ViewListIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="link"
						placeholder="Link"
						position="alone"
						autoComplete="off"
						icon={LinkIcon}
						onInput={handleOnInput}
					/>

					<TextInput
						name="imageUrl"
						placeholder="Image URL"
						position="alone"
						autoComplete="off"
						icon={PhotographIcon}
						onInput={handleOnInput}
					/>

					<ColourPickerButton
						name="colour"
						colour={colour}
						setColour={handleColourChange}
					/>

					<ErrorList errors={errors}/>

					<SubmitButton
						text={loading ? "Creating..." : "Create"}
						loading={loading}
					/>
				</form>
			</div>
		</div>
	);
};

export default NewSkillForm;
