import React from "react";

const TextAreaInput = ({
	name,
	placeholder,
	position = "middle",
	autoComplete = "on",
	disabled,
	icon: Icon,
	onInput,
	defaultValue,
	rows = 5,
	readOnly
}) => {
	let borderClass = "";
	let iconClass = "";

	if (position === "first")
		borderClass = "rounded-t-md";
	else if (position === "last")
		borderClass = "rounded-b-md";
	else if (position === "alone")
		borderClass = "rounded-t-md rounded-b-md";

	if (Icon)
		iconClass = " pl-14";

	return (
		<>
			<label className="relative focus-within:text-indigo-500
				focus-within:z-10 block">

				{Icon ?
					<Icon className="pointer-events-none w-8 h-7 absolute
					top-1/2 transform -translate-y-1/2 left-3"/> : ""
				}

				<textarea
					autoComplete={autoComplete}
					name={name}
					className={`form-input appearance-none rounded-none block w-full px-4
					py-3 bg-dark-650 border border-dark-500 placeholder-gray-200 text-white
					focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm
					disabled:opacity-60 ` + borderClass + iconClass}
					placeholder={placeholder}
					disabled={disabled}
					onInput={onInput}
					defaultValue={defaultValue}
					rows={rows}
					readOnly={readOnly}
				/>
			</label>
		</>
	);
};

export default TextAreaInput;
