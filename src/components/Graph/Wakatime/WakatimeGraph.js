import React from "react";
import {
	Area,
	AreaChart,
	XAxis,
	YAxis,
	Tooltip,
	ReferenceLine,
	ResponsiveContainer
} from "recharts";
import WakatimeGraphTooltip from "./WakatimeGraphTooltip";
import Skeleton from "react-loading-skeleton";

const WakatimeGraph = ({data, loading, dailyAverage}) => {
	if (loading) {
		return (
			<ResponsiveContainer width="80%" height={400} className="lg:mx-auto mb-6">
				<Skeleton/>
			</ResponsiveContainer>
		);
	}

	return (
		<ResponsiveContainer width="80%" height={400} className="lg:mx-auto mt-6">
			<AreaChart data={data}>
				<defs>
					<linearGradient id="colourHours" x1="0" y1="0" x2="0" y2="1">
						<stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
						<stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
					</linearGradient>
				</defs>
				<XAxis dataKey="name"/>
				<YAxis/>
				<Tooltip
					contentStyle={{
						backgroundColor: "#303030",
						border: "1px solid #202020"
					}}
					content={<WakatimeGraphTooltip/>}
				/>
				<ReferenceLine y={dailyAverage} label={{value: "Daily Average", fill: "#ffffff"}}
					stroke="lightgreen" strokeDasharray="5 5"/>
				<Area type="monotone" dataKey="Hours" stroke="#8884d8"
					fillOpacity={1} fill="url(#colourHours)"/>
			</AreaChart>
		</ResponsiveContainer>
	);
};

export default WakatimeGraph;
