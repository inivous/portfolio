import React from "react";

const MobilePageMoveRightButton = ({onClick, disabled}) => {
	if (disabled) {
		return (
			<a className="ml-3 relative inline-flex items-center px-4 py-2 rounded-md opacity-50
				bg-dark-750 border-dark-600 border text-sm font-medium text-white hover:bg-dark-800">
				Next
			</a>
		);
	}

	return (
		<a
			className="ml-3 relative inline-flex items-center px-4 py-2 rounded-md cursor-pointer
				bg-dark-750 border-dark-600 border text-sm font-medium text-white hover:bg-dark-800"
			onClick={onClick}
		>
			Next
		</a>
	);
};

export default MobilePageMoveRightButton;
