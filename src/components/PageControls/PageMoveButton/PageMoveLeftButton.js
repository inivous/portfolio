import React from "react";
import {ChevronLeftIcon} from "@heroicons/react/solid";

const PageMoveLeftButton = ({onClick, disabled}) => {
	if (disabled) {
		return (
			<a
				className="relative inline-flex items-center px-2 py-2 rounded-l-md opacity-50
					bg-dark-700 border-dark-600 border text-sm font-medium text-white">
				<ChevronLeftIcon className="h-5 w-5"/>
			</a>
		);
	}

	return (
		<a
			className="relative inline-flex items-center px-2 py-2 rounded-l-md cursor-pointer
				bg-dark-700 border-dark-600 border text-sm font-medium text-white hover:bg-dark-750"
			onClick={onClick}
		>
			<ChevronLeftIcon className="h-5 w-5"/>
		</a>
	);
};

export default PageMoveLeftButton;
