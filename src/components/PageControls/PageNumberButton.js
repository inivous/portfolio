import React from "react";

const PageNumberButton = ({pageNumber, disabled, active, onClick}) => {
	if (disabled) {
		return (
			<span className="bg-dark-700 border-dark-600 border text-white hidden
				md:inline-flex relative items-center px-4 py-2 text-sm font-medium">
				{pageNumber}
			</span>
		);
	}

	let activeClass = "";

	if (active)
		activeClass = "bg-dark-750";

	return (
		<a
			className={`z-10 bg-dark-700 border-dark-600 border text-white relative cursor-pointer
				inline-flex items-center px-4 py-2 text-sm font-medium hover:bg-dark-750 ${activeClass}`}
			onClick={() => onClick(pageNumber)}
		>
			{pageNumber}
		</a>
	);
};

export default PageNumberButton;
