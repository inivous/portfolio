import React from "react";
import Link from "../Link/Link";

const SpotifyArtist = ({name, url}) => {
	return (
		<Link text={name} href={url} textClass="text-white"/>
	);
};

export default SpotifyArtist;
