import React from "react";
import Link from "../Link/Link";

const SpotifyTrack = ({name, url}) => {
	return (
		<h2 className="text-white text-base sm:text-xl lg:text-base
			xl:text-xl font-semibold truncate">
			<Link text={name} href={url}
				title={name}/>
		</h2>
	);
};

export default SpotifyTrack;
