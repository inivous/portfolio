import React from "react";
import {DateTime} from "luxon";
import {pluralise} from "../../../helpers/general";

const DurationDate = ({startDate, startDateString, endDate, endDateString, includeDiff = true}) => {
	let date = DateTime.fromMillis(startDate);
	let diffDate = endDate ? DateTime.fromMillis(endDate) : DateTime.now();
	let diff = diffDate.diff(date, ["years", "months", "days"]);
	let diffObject = diff.toObject();
	let diffArray = [];

	if (diffObject.years)
		diffArray.push(`${diffObject.years} ${pluralise(diffObject.years, "year")}`);

	if (diffObject.months)
		diffArray.push(`${diffObject.months} ${pluralise(diffObject.months, "month")}`);

	if (diffObject.days) {
		// Since days is the smallest unit, Luxon doesn't round this
		let days = Math.round(diffObject.days);
		diffArray.push(`${days} ${pluralise(days, "day")}`);
	}

	return (
		<>
			<p>{startDateString} - {endDateString || "Present"}</p>

			{includeDiff ?
				<p>{diffArray.join(", ")}</p> :
				""
			}
		</>
	);
};

export default DurationDate;
