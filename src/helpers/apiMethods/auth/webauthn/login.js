import axios from "axios";
import wrapAxiosRequest from "../../wrapAxiosRequest";

const login = async (username) => {
	return wrapAxiosRequest(axios.get(`/api/auth/webauthn/login?username=${username}`));
};

export default login;
