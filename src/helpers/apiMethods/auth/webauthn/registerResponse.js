import axios from "../../axiosWithRefreshToken";
import wrapAxiosRequest from "../../wrapAxiosRequest";

const registerResponse = async (body) => {
	return wrapAxiosRequest(axios.post("/api/auth/webauthn/registerResponse", body));
};

export default registerResponse;
