import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const deleteEducation = async (educationId) => {
	return wrapAxiosRequest(axios.delete(`/api/education/${educationId}`));
};

export default deleteEducation;
