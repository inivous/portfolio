import axios from "axios";
import wrapAxiosRequest from "./../wrapAxiosRequest";

const serverSideRefreshToken = async (id) => {
	return wrapAxiosRequest(axios.get(process.env.BASE_API_URL + `/api/project/${id}`));
};

export default serverSideRefreshToken;
