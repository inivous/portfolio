import axios from "../axiosWithRefreshToken";
import wrapAxiosRequest from "../wrapAxiosRequest";

const editSkill = async (id, name, type, link, imageUrl, colour) => {
	return wrapAxiosRequest(axios.put(`/api/skill/${id}`, {
		name,
		type,
		link,
		imageUrl,
		colour
	}));
};

export default editSkill;

