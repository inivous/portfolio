import {getJson, setexJson} from "./redis";
import axios from "axios";
import {DateTime} from "luxon";

const getDateString = (date) => {
	let dateString = date.getUTCDate().toString().padStart(2, "0");
	let monthString = (date.getUTCMonth() + 1).toString().padStart(2, "0");

	return `${date.getUTCFullYear()}-${monthString}-${dateString}`;
};

export const getCalendarStatistics = async () => {
	let cachedStats = await getJson("gitlabCalendarStatistics");

	// Don't use cached stats for dev
	if (cachedStats && process.env.ENVIRONMENT !== "dev")
		return cachedStats;

	let calendar = (await axios
		.get(`https://gitlab.com/users/${process.env.GITLAB_USERNAME}/calendar.json`)).data;

	let date = new Date();
	let days = 0;
	let contributionStreak = 0;
	let totalContributions = 0;
	let highestDay = {
		date: "",
		count: -1
	};
	let streakBroken = false;

	// If today isn't included, set it to 0 commits
	if (!calendar[getDateString(date)])
		calendar[getDateString(date)] = 0;

	// Go backwards from today counting the commit streak
	for (let i = 0; i < 365; i++) {
		let dateString = getDateString(date);
		let contributionsToday = calendar[dateString];

		if (Object.prototype.hasOwnProperty.call(calendar, dateString)) {
			if (!streakBroken) {
				days += 1;
				contributionStreak += contributionsToday;
			}

			totalContributions += contributionsToday;

			if (contributionsToday > highestDay.count) {
				highestDay = {
					date: dateString,
					count: contributionsToday
				};
			}
		} else
			streakBroken = true;

		date.setDate(date.getDate() - 1);
	}

	let statsObject = {
		contributionStreak: {
			days,
			contributions: contributionStreak
		},
		totalContributions,
		highestDay
	};

	await setexJson("gitlabCalendarStatistics", statsObject, 3600);

	return statsObject;
};

export const getLastContributionTimestamp = async () => {
	// eslint-disable-next-line max-len
	let url = `https://gitlab.com/api/v4/users/${process.env.GITLAB_USER_ID}/events?action_type=pushed&per_page=1`;

	let response = (await axios.get(url, {
		headers: {
			"PRIVATE-TOKEN": process.env.GITLAB_AUTH_TOKEN
		}
	})).data;

	let [latestContribution] = response;

	return DateTime.fromISO(latestContribution.created_at)
		.toMillis();
};
