export const pages = [
	{
		url: "/",
		name: "Home"
	},
	{
		url: "/stats",
		name: "Stats"
	},
	{
		url: "/projects",
		name: "Projects"
	},
	{
		url: "/skills",
		name: "Skills"
	},
	{
		url: "/education",
		name: "Education"
	},
	{
		url: "/experience",
		name: "Experience"
	},
	{
		url: "/contact",
		name: "Contact"
	}
];

export const profilePages = [
	{
		url: "/user/settings",
		name: "User Settings"
	},
	{
		url: "/admin",
		name: "Admin Dashboard"
	},
	{
		url: "/logout",
		name: "Logout"
	}
];

export const adminPages = [
	{
		url: "/admin/projects",
		name: "Projects",
		description: "Create, edit and delete projects",
		icon: ["fas", "project-diagram"]
	},
	{
		url: "/admin/skills",
		name: "Skills",
		description: "Create, edit and delete skills",
		icon: ["fas", "brain"]
	},
	{
		url: "/admin/education",
		name: "Education",
		description: "Create and delete education history",
		icon: ["fas", "graduation-cap"]
	},
	{
		url: "/admin/experience",
		name: "Experience",
		description: "Create and delete past experiences",
		icon: ["fas", "list"]
	},
	{
		url: "/admin/contacts",
		name: "Contacts",
		description: "Create and remove contacts",
		icon: ["fas", "id-card"]
	}
];

export const projectSortByOptions = [
	{
		text: "Date Added",
		value: "dateAdded"
	},
	{
		text: "Name",
		value: "name"
	},
	{
		text: "Open Source",
		value: "openSource"
	},
	{
		text: "ID",
		value: "id"
	}
];

export const sortByDirectionOptions = [
	{
		text: "Descending",
		value: "DESC"
	},
	{
		text: "Ascending",
		value: "ASC"
	}
];

export const months = [
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
];

export const daysInMonths = [
	31,
	28,
	31,
	30,
	31,
	30,
	31,
	31,
	30,
	31,
	30,
	31
];
