import {serialize} from "cookie";
import {verifyJwtAsync} from "../helpers/auth";
import serverSideRefreshToken from "../helpers/apiMethods/serverSideRefreshToken";

export const useAuth = async (ctx) => {
	let {
		token,
		refreshToken
	} = ctx.req.cookies;

	if (!token)
		return null;

	let decoded;

	try {
		decoded = await verifyJwtAsync(token, true);
	} catch (err) {
		return null;
	}

	if (!decoded)
		return null;

	if (decoded.exp < new Date().getTime() / 1000) {
		let {error, body} = await serverSideRefreshToken(token, refreshToken);

		if (error) {
			let date = new Date(0);

			ctx.res.setHeader("Set-Cookie", [
				serialize("token", "", {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: date
				}),
				serialize("refreshToken", "", {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: date
				})
			]);

			return null;
		}

		let date = new Date();
		date.setDate(date.getDate() + 31);

		ctx.res.setHeader("Set-Cookie", [
			serialize("token", body.token, {
				path: "/",
				domain: process.env.AUTH_COOKIE_DOMAIN,
				secure: true,
				httpOnly: true,
				expires: date
			})
		]);
	}

	return decoded;
};
