import React from "react";
import ErrorPage from "../components/Errors/ErrorPage";

const NotFound = () => {
	return (
		<ErrorPage statusCode={404}/>
	);
};

export default NotFound;
