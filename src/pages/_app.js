import React from "react";
import Head from "next/head";
import Router from "next/router";
import "../assets/scss/global.scss";
import "tailwindcss/tailwind.css";
import "../assets/scss/react-vertical-timeline-component/VerticalTimeline.min.css";
import { SkeletonTheme } from "react-loading-skeleton";
import { cache, SWRConfig } from "swr";
import axios from "axios";
import ProgressBar from "@badrap/bar-of-progress";
import { transitions, positions, types, Provider as AlertProvider } from "react-alert";
import NavBar from "../components/NavBar/NavBar";
import Toast from "../components/Toast/Toast";

axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.NEXT_PUBLIC_BASE_API_URL;

const progress = new ProgressBar({
	size: 2,
	color: "#ffffff",
	className: "top-loading-bar",
	delay: 100
});

const AlertTemplate = ({options, message}) => {
	return (
		<Toast
			body={message}
			type={options.type}
		/>
	);
};

Router.events.on("routeChangeStart", progress.start);
Router.events.on("routeChangeComplete", progress.finish);
Router.events.on("routeChangeError", progress.finish);

const App = ({ Component, pageProps }) => {
	const Layout = Component.layout || (({ children }) => <>{children}</>);

	const fetcher = async (url) => {
		try {
			return (await axios.get(url)).data;
		} catch (err) {
			const error = {};

			if (!err.response) {
				error.statusCode = 0;
				error.data = {
					code: err.code,
					displayMessage: "An unknown error occurred"
				};
			} else {
				error.statusCode = err.response.status;
				error.data = err.response.data;
			}

			throw error;
		}
	};

	const handleError = (err) => {
		if (err.statusCode === 401)
			cache.clear();
	};

	const alertOptions = {
		offset: "12px",
		timeout: 5000,
		position: positions.TOP_RIGHT,
		type: types.INFO,
		transition: transitions.FADE,
		containerStyle: {
			zIndex: 1500
		}
	};

	return (
		<AlertProvider template={AlertTemplate} {...alertOptions}>
			<SWRConfig value={{ fetcher: fetcher, onError: handleError, shouldRetryOnError: false }}>
				<SkeletonTheme color="#202020" highlightColor="#323232">
					<Head>
						<title>dantarr.dev</title>
						<meta httpEquiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
						<meta
							name="keywords"
							content={
								"dan, daniel, tarr, portfolio, projects, software, developer, engineer"
							}
						/>
						<meta content='width=device-width, initial-scale=1.0,
							maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport'/>
						<link rel="shortcut icon"
							href={`${process.env.NEXT_PUBLIC_CDN_URL}/assets/static/img/icons/icon.ico`}/>
						<link rel="icon" sizes="16x16 32x32 64x64"
							href={`${process.env.NEXT_PUBLIC_CDN_URL}/assets/static/img/icons/icon.ico`}/>
						<meta
							property="og:title" content="Dan Tarr - Portfolio"
							key="title"
						/>
						<meta property="og:description" content={ "" } key="description"/>
						<meta
							property="og:image"
							content={`${process.env.NEXT_PUBLIC_CDN_URL}/assets/static/img/logo.png`}
							key="image"
						/>
						<meta name="theme-color" content="#151515" key="colour"/>
					</Head>
					<Layout>
						<NavBar/>
						<Component {...pageProps} />
					</Layout>
				</SkeletonTheme>
			</SWRConfig>
		</AlertProvider>
	);
};

export default App;
