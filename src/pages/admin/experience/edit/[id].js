import React from "react";
import SmallHeader from "../../../../components/Header/SmallHeader";
import EditExperience from "../../../../components/Admin/Experience/EditExperience";
import {useAuth} from "../../../../lib/useAuth";
import ErrorPage from "../../../../components/Errors/ErrorPage";

const EditExperiencePage = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Edit Experience"
			/>

			<EditExperience/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default EditExperiencePage;
