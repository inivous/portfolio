import React from "react";
import SmallHeader from "../../../../components/Header/SmallHeader";
import EditProject from "../../../../components/Admin/Projects/EditProject";
import {useAuth} from "../../../../lib/useAuth";
import ErrorPage from "../../../../components/Errors/ErrorPage";

const EditProjectPage = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Edit Project"
			/>

			<EditProject/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default EditProjectPage;
