import React from "react";
import SmallHeader from "../../../../components/Header/SmallHeader";
import EditSkill from "../../../../components/Admin/Skills/EditSkill";
import {useAuth} from "../../../../lib/useAuth";
import ErrorPage from "../../../../components/Errors/ErrorPage";

const EditSkillPage = ({error}) => {
	if (error)
		return <ErrorPage statusCode={error}/>;

	return (
		<>
			<SmallHeader
				title="Edit Skill"
			/>

			<EditSkill/>
		</>
	);
};

export const getServerSideProps = async (ctx) => {
	const data = await useAuth(ctx);

	if (!data)
		return {props: {error: 404}};

	return {props: {}};
};

export default EditSkillPage;
