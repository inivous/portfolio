import {
	internalServerError,
	methodNotAllowed
} from "../../../lib/responses";
import {
	signJwtAsync,
	verifyJwtAsync
} from "../../../helpers/auth";
import RefreshTokenSchema from "../../../schemas/API/RefreshTokenSchema";
import {serialize} from "cookie";

const handler = async (req, res) => {
	try {
		if (req.method === "POST") {
			let validationResult = RefreshTokenSchema.validate(req.cookies, {
				abortEarly: false,
				stripUnknown: true
			});

			if (validationResult.error) {
				return res.status(400).send({
					error: true,
					displayMessage: "There are validation errors within the cookies",
					errors: validationResult.error.details,
					code: "COOKIES_VALIDATION_ERROR"
				});
			}

			let {
				token,
				refreshToken
			} = req.cookies;

			let tokenPayload;
			let refreshTokenPayload;

			try {
				tokenPayload = await verifyJwtAsync(token, true);
				refreshTokenPayload = await verifyJwtAsync(refreshToken);
			} catch (err) {
				return res.status(401).send({
					error: true,
					displayMessage: "The provided authentication token or refresh token is invalid",
					code: "INVALID_TOKEN"
				});
			}

			if (tokenPayload.tokenId !== refreshTokenPayload.tokenId) {
				return res.status(400).send({
					error: true,
					displayMessage: "Invalid refresh token",
					code: "INVALID_REFRESH_TOKEN"
				});
			}

			let authToken = await signJwtAsync({
				userId: tokenPayload.userId,
				tokenId: tokenPayload.tokenId
			});

			let cookieExpiryDate = new Date();
			cookieExpiryDate.setDate(cookieExpiryDate.getDate() + 31);

			res.setHeader("Set-Cookie", [
				serialize("token", authToken, {
					path: "/",
					domain: process.env.AUTH_COOKIE_DOMAIN,
					secure: true,
					httpOnly: true,
					expires: cookieExpiryDate
				})
			]);

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				token: authToken
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		return res.status(500).send(internalServerError);
	}
};

export default handler;
