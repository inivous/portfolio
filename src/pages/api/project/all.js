import {
	internalServerError,
	methodNotAllowed
} from "../../../lib/responses";
import {
	query,
	queryBuilder
} from "../../../lib/database";
import authMiddleware from "../../../middleware/authMiddleware";

const handler = async (req, res) => {
	try {
		if (req.method === "GET") {
			let resultsQuery = queryBuilder.table("projects")
				.select(["projects.*"])
				.orderBy("name", "ASC")
				.toSQL();

			let projects = await query(resultsQuery.sql, resultsQuery.bindings);

			let projectIds = projects.map(project => project.id);

			let projectSkillsQuery = queryBuilder.table("projectSkills")
				.select("*")
				.whereIn("projectId", projectIds)
				.toSQL();

			let projectSkills = await query(projectSkillsQuery.sql, projectSkillsQuery.bindings);

			for (let project of projects) {
				project.openSource = project.openSource === 1;
				project.contributors = JSON.parse(project.contributors);
				project.skills = projectSkills.filter(projectSkill =>
					projectSkill.projectId === project.id);
				project.imageUrls = project.imageUrls.split(",");
			}

			return res.status(200).send({
				error: false,
				code: "SUCCESS",
				projects
			});
		}

		return res.status(405).send(methodNotAllowed(req.method, ["POST"]));
	} catch (err) {
		console.log(err);
		return res.status(500).send(internalServerError);
	}
};

export default authMiddleware(handler, ["GET"]);
