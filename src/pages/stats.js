import React from "react";
import SmallHeader from "../components/Header/SmallHeader";
import StatsContainer from "../components/Stats/StatsContainer";

const Stats = () => {
	return (
		<>
			<SmallHeader
				title="Stats"
				subtitle="A few interesting statistics about my activity"
			/>

			<StatsContainer/>
		</>
	);
};

export default Stats;
