import Joi from "joi";

export default Joi.object({
	title: Joi.string()
		.max(64)
		.required(),
	text: Joi.string()
		.max(512)
		.required(),
	link: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(1024)
		.allow(null)
		.required(),
	baseSiteLink: Joi.string()
		.uri({
			scheme: ["http", "https"]
		})
		.max(512)
		.required(),
	icon: Joi.string()
		.max(128),
	iconColour: Joi.when("icon", {
		is: Joi.exist(),
		then: Joi.string()
			.length(6)
			.hex()
			.required()
	})
});
