import Joi from "joi";
import Contributors from "./ContributorsSchema";

export default Joi.object({
	name: Joi.string()
		.max(128)
		.required()
		.label("Name"),
	shortDescription: Joi.string()
		.max(256)
		.required()
		.label("Short Description"),
	longDescription: Joi.string()
		.max(16777215)
		.required()
		.label("Long Description"),
	openSource: Joi.boolean()
		.required()
		.label("Open Source"),
	repository: Joi.string()
		.uri({
			domain: {
				tlds: ["com"]
			},
			scheme: "https"
		})
		.allow(null)
		.max(1024)
		.required()
		.label("Repository"),
	contributors: Joi.array()
		.items(Contributors)
		.required()
		.label("Contributors"),
	imageUrls: Joi.array()
		.items(Joi.string()
			.uri({
				scheme: "https"
			})
			.allow(null)
			.max(512)
			.label("Image URL")
		)
		.unique()
		.required(),
	skillIds: Joi.array()
		.items(Joi.string()
			.length(22)
		)
		.required()
		.label("Skill IDs")
});
