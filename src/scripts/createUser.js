const knex = require("knex");
const {randomBytes} = require("crypto");
const bcrypt = require("bcrypt");
const prompts = require("prompts");
const saltRounds = 10;

const queryBuilder = knex({
	client: "mysql"
});

const generateId = (prefix) => {
	let id = randomBytes(8)
		.toString("hex");

	return `${prefix}_${id}`;
};

(async () => {
	let {username, password, avatarUrl} = await prompts([
		{
			type: "text",
			name: "username",
			message: "What is the username of the account?",
			validate: value => value.length > 128 ?
				"Username must be less than or equal to 128 characters" : true
		},
		{
			type: "password",
			name: "password",
			message: "What is the password of the account?"
		},
		{
			type: "text",
			name: "avatarUrl",
			message: "What is the URL of the accounts avatar?",
			validate: value => value.length > 256 ?
				"Avatar URL must be less than or equal to 256 characters" : true
		}
	]);

	let hashedPassword = await bcrypt.hash(password, saltRounds);

	let user = {
		ID: generateId("user"),
		Username: username,
		Password: hashedPassword,
		AvatarURL: avatarUrl
	};

	let query = queryBuilder.table("users")
		.insert({
			id: user.ID,
			username: user.Username,
			password: user.Password,
			avatarUrl: user.AvatarURL
		})
		.toQuery();

	console.log("Insert the following information into the users table.");
	console.table([user], Object.keys(user));

	console.log("Or run the following SQL query:\n");
	console.log(query);
})();
